Select * 
from employees
WHERE salary > 40000
	AND salary < 100000
	AND department = 'Automotive';
	
-- This will render the same result

SELECT *
FROM employees
WHERE salary BETWEEN 40000 and 100000
	AND department = 'Automotive'
	--AND sdlfkjsdflkjsdflkj
	;
	
-- ORDER BY

SELECT first_name, last_name, hire_date
FROM employees
WHERE department = 'Automotive'
ORDER BY hire_date asc;

--department list
SELECT DISTINCT department
FROM employees
ORDER BY department
FETCH FIRST 10 ROW ONLY;

SELECT DISTINCT department
FROM employees
ORDER BY department
LIMIT 10;

--AS
SELECT first_name as "VERY FIRST NAME", last_name as "VERY LAST NAME"
from employees;

-- 5 most paid emloyees
SELECT first_name, last_name, salary
FROM employees
ORDER BY salary DESC
LIMIT 5;




	
	
