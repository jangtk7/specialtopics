SELECT UPPER(first_name) as "first", LOWER(last_name) as "last"
FROM employees;


--CONCAT
SELECT first_name ||' '|| last_name as "FULL_NAME", (salary > 100000)
FROM employees;

-- Boolean is case sensitive
SELECT ('P' in ('p', 'c'));

--More functions
SELECT SUBSTRING('sdlfkjsdflk sdfkjsdlfkjsd' FROM 1 FOR 5);

SELECT REPLACE(department, 'Grocery', 'Food')
FROM employees;

--EXTRANTING domain only
SELECT email, SUBSTRING(email, POSITION('@' IN email)+1, (POSITION('.' IN email)-POSITION('@' IN email)+1)-2) as domain
FROM employees;


--COALESCE
SELECT COALESCE(email, 'NONE')
FROM employees;

--MAX

SELECT MAX(salary)
FROM employees;
-- equals
SELECT salary
FROM employees
ORDER by salary DESC
LIMIT 1;

SELECT COUNT(email)
FROM employees;

SELECT department, COUNT(department)
FROM employees
GROUP BY department;

SELECT *
FROM employees
WHERE department NOT IN ('Sports', 'Tools');




