SELECT department, SUM(salary)
FROM employees
GROUP BY department;

/*grouping department by total expense */
SELECT department, SUM(salary) as expense, COUNT(*)
FROM employees
GROUP BY department
HAVING COUNT(*) > 35
ORDER BY COUNT(*) DESC;


/* List of employees that have same last name */

SELECT last_name, count(*)
FROM employees
GROUP BY last_name
HAVING COUNT(*) > 1;

/* email domain */
SELECT SUBSTRING(email, position('@' IN email)+1) as email_domain, count(*)
FROM employees
GROUP BY SUBSTRING(email, position('@' IN email)+1)
ORDER BY count(*) DESC;

/* group by gender and region */
SELECT gender, region_id, MIN(salary), MAX(salary), round(AVG(salary))
FROM employees
GROUP BY gender, region_id	
ORDER BY gender, region_id;

/* AS */
SELECT e.gender
FROM employees AS e, departments AS d
