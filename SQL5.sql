/* Subqueries*/

SELECT *
FROM (SELECT first_name, salary
	  FROM employees
	  WHERE salary > 150000) AS table1;



SELECT *
FROM employees
WHERE department IN (SELECT department FROM departments);

SELECT *
FROM (SELECT department FROM departments);




SELECT first_name, last_name, salary, 
		(SELECT MAX(salary) FROM employees) AS max_salary, 
		((SELECT MAX(salary) FROM employees) - salary) AS diff 
FROM employees


/*Assignments*/

SELECT *
FROM employees
WHERE department IN (SELECT department FROM departments WHERE division = 'Electronics')


SELECT *
FROM employees
WHERE salary >130000
		AND region_id in (SELECT region_id FROM regions WHERE country IN ('Asia', 'Canada'))



