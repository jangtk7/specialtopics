
(Select department
From departments
WHERE division = 'Kids')

(SELECT hire_date
FROM employees
WHERE department = 'Maintenance')

SELECT *
FROM employees
WHERE department = ANY (Select department
						From departments
						WHERE division = 'Kids')
AND

	hire_date > ALL (SELECT hire_date
						FROM employees
						WHERE department = 'Maintenance')


SELECT salary, COUNT(*)
FROM employees
GROUP BY salary
ORDER BY count(*) DESC, salary DESC



/*CASE WEHN*/

SELECT t1.c1, COUNT(*)
FROM
(SELECT first_name, last_name, salary,
		CASE WHEN salary > 100000 THEN 'Rich'
			 WHEN salary < 100000 THEN 'Poor'
			 ELSE 'NOT PAID'
			 END AS c1
FROM employees) t1
GROUP BY t1.c1

/*Transpose*/

SELECT department, COUNT(*)
FROM employees
WHERE department IN ('Sports', 'Tools', 'Clothing', 'Computers')
GROUP BY department


SELECT SUM(CASE WHEN department = 'Sports' THEN 1 ELSE 0 END) AS sports,
	    SUM(CASE WHEN department = 'Tools' THEN 1 ELSE 0 END) AS tools,
		 SUM(CASE WHEN department = 'Clothing' THEN 1 ELSE 0 END) AS clothings,
		  SUM(CASE WHEN department = 'Computers' THEN 1 ELSE 0 END) AS computers
FROM employees



/*more transpose*/
CREATE VIEW tt AS
(
SELECT COUNT(t1. region1)+COUNT(t1. region2)+COUNT(t1. region3) AS US,
	   COUNT(t1. region4)+COUNT(t1. region5) AS ASIA,
	   COUNT(t1. region6)+COUNT(t1. region7) AS CANADA
FROM
(SELECT first_name,
CASE WHEN region_id=1 THEN (SELECT country FROM regions WHERE region_id=1) END AS region1,
CASE WHEN region_id=2 THEN (SELECT country FROM regions WHERE region_id=2) END AS region2,
CASE WHEN region_id=3 THEN (SELECT country FROM regions WHERE region_id=3) END AS region3,
CASE WHEN region_id=4 THEN (SELECT country FROM regions WHERE region_id=4) END AS region4,
CASE WHEN region_id=5 THEN (SELECT country FROM regions WHERE region_id=5) END AS region5,
CASE WHEN region_id=6 THEN (SELECT country FROM regions WHERE region_id=6) END AS region6,
CASE WHEN region_id=7 THEN (SELECT country FROM regions WHERE region_id=7) END AS region7
FROM employees
) AS t1
)

SELECT US +ASIA + CANADA AS world
FROM tt;
