SELECT department
FROM departments d
WHERE 38 < (SELECT count(*) 
			FROM employees e
		   	WHERE e.department= d.department)





/* CREATE VIEW method */
CREATE VIEW grouped AS
(SELECT department, count(*) AS num
		FROM employees
		GROUP BY department)

SELECT department
FROM grouped
WHERE count > 38


/* EXERCISE */
SELECT department, first_name, salary, (CASE WHEN salary = max_sal THEN 'HIGHEST'
											 WHEN salary = min_sal THEN 'LOWEST' END) AS highlow
FROM (
SELECT department, first_name, salary, 
	(SELECT MAX(salary) 
	 FROM employees e2 
	 WHERE e2.department = e1.department) as max_sal,
 
	 (SELECT MIN(salary) 
	 FROM employees e2 
	 WHERE e2.department = e1.department) as min_sal
FROM employees e1
) aa
WHERE salary IN (max_sal, min_sal)
ORDER BY department




