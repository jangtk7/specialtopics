SELECT first_name, last_name, country
FROM employees as e, regions as r
WHERE e.region_id = r.region_id

SELECT first_name, email, division, country
FROM employees AS e, departments AS d, regions AS r
WHERE e.department = d.department 
		AND e.region_id = r.region_id
		AND email IS NOT NULL
	
	
/*Inner Join*/	
SELECT first_name, last_name, country
FROM employees as e 
Inner Join regions as r
ON e.region_id = r.region_id	


SELECT first_name, email, division, country
FROM employees AS e
Inner Join departments AS d
ON e.department = d.department 
Inner Join regions AS r
ON e.region_id = r.region_id
WHERE email IS NOT NULL


SELECT COUNT(DISTINCT department)
FROM employees

SELECT COUNT(DISTINCT department)
FROM departments

SELECT DISTINCT e. department, d.department
FROM employees e
left Join departments d
ON e.department = d.department
WHERE d.department IS NULL

SELECT DISTINCT e. department, d.department
FROM employees e
RIGHT Join departments d
ON e.department = d.department
WHERE e.department IS NULL


SELECT DISTINCT e. department, d.department
FROM employees e
LEFT OUTER Join departments d
ON e.department = d.department


SELECT DISTINCT e. department, d.department
FROM employees e
RIGHT OUTER Join departments d
ON e.department = d.department


/* Union*/
SELECT DISTINCT department
FROM employees
UNION
SELECT DISTINCT department
FROM departments

/*UNION ALL*/
/*Does not get rid of duplicates*/
SELECT DISTINCT department
FROM employees
UNION ALL
SELECT DISTINCT department
FROM departments



/*Except*/
SELECT DISTINCT department
FROM employees
Except
SELECT DISTINCT department
FROM departments

/*We can do the same thing with JOIN*/
SELECT DISTINCT department
FROM departments
Except
SELECT DISTINCT department
FROM employees


SELECT department, COUNT(*) 
FROM employees
GROUP BY department
UNION ALL
SELECT 'TOTAL', count(*)
FROM employees

/*CROSS JOIN*/

SELECT COUNT(*) 
FROM (SELECT * FROM employees as e1, employees as e2) as sub
 /*same as... */
SELECT COUNT(*)
FROM employees as e1
CROSS JOIN employees as e2


SELECT *
FROM employees
LIMIT 5


/*MAX and MIN hire_date*/
(SELECT e.first_name, e.department, hire_date, country
FROM employees as e
JOIN regions as r
on e.region_id = r.region_id
ORDER BY hire_date DESC
LIMIT 1)
UNION
(SELECT e.first_name, e.department, hire_date, country
FROM employees as e
JOIN regions as r
on e.region_id = r.region_id
ORDER BY hire_date
LIMIT 1)


/*Same query using CREATE VIEW*/
CREATE VIEW max_date AS (SELECT e.first_name, e.department, hire_date, country
FROM employees as e
JOIN regions as r
on e.region_id = r.region_id
ORDER BY hire_date DESC
LIMIT 1)

(SELECT * FROM max_date)
UNION
(SELECT e.first_name, e.department, hire_date, country
FROM employees as e
JOIN regions as r
on e.region_id = r.region_id
ORDER BY hire_date
LIMIT 1)


