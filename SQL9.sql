SELECT first_name, last_name, department, 
count(*) OVER(PARTITION BY department)
FROM employees

SELECT first_name, last_name, department, 
count(*) OVER()
FROM employees
WHERE region_id=1 /*WHERE clause will determine the "universe" of OVER() function */


/*Two queries return exact same tables*/
SELECT first_name, last_name, department, hire_date,
SUM(salary) OVER(ORDER BY hire_date RANGE BETWEEN UNBOUNDED PRECEDING 
				 					AND CURRENT ROW)
FROM employees
EXCEPT
SELECT first_name, last_name, department, hire_date,
SUM(salary) OVER(ORDER BY hire_date)
FROM employees


/*partitions by department*/
SELECT first_name, last_name, department, hire_date,
SUM(salary) OVER(PARTITION BY department ORDER BY hire_date)
FROM employees

SELECT first_name, last_name, department, hire_date, salary,
AVG(salary) OVER (ORDER BY hire_date ASC ROWS 9 PRECEDING)
FROM employees




/*USING correlated Subquery*/
SELECT first_name, department, 
	(SELECT MAX(salary) FROM employees WHERE department = e1.department) as salary
FROM employees e1
WHERE salary = (SELECT MAX(salary) FROM employees e2 WHERE e1.department = e2.department)


/*USING window function*/
SELECT first_name, department, salary
FROM
(SELECT first_name, department, salary, 
RANK() OVER (PARTITION BY department ORDER BY salary DESC)
FROM employees) a
WHERE rank = 1


/*LEAD and LAG*/

SELECT first_name, last_name, salary,
LEAD(salary) OVER(ORDER BY salary),
LAG(salary) OVER()
FROM employees


/*GROUP BY options*/
SELECT region, country
FROM regions
GROUP BY  GROUPING SETS(region, country)

SELECT region, country
FROM regions
GROUP BY ROLLUP(region, country)

SELECT region, country
FROM regions
GROUP BY CUBE(region, country)

/*deliminating in SQL*/
SELECT SPLIT_PART(region::TEXT,' ',1),
SPLIT_PART(region::TEXT,' ',2)
FROM regions



